from __future__ import print_function

import sys
import re

# This script parses the TiddlyWiki timeline.html, to create a
# CSV output where the X axis is time ("Month/Year" format)
# and the Y axis is Authors, and the entry in a given [X][Y] index
# is the running total of presentations by that author

#NOTE: As a reminder to myself, I use flourish.studio to then visualize this data

# Returns a "month/year" string from a given line
def extract_month_year(line):
    # Setting initial values, which, while within the acceptable
    # range, are not places where talks actually took place
    # Therefore anything showing up here should be considered
    # invalid, and the timeline.html needs to be fixed for it
    month = 1
    year = 2001

    toks = line.split("[[")
    for t in toks:
        toks2 = t.split("]]")
        for tt in toks2:
            x = re.search(r"Year: .*", tt)
            if(x != None):
                year = x.string.split("Year: ")[1]
            x = re.search(r"Month: [0-1][0-9]", tt)
            if(x != None):
                month = x.string.split("Month: ")[1]

#    print("returning {:02d}/{}".format(int(month),year))
    return "{:02d}/{}".format(int(month),year)

# Returns a list of authors for this line (could be empty)
def extract_author_list(line):
    tal = [] # temp author list
    toks = line.split("[[")
    for t in toks:
        toks2 = t.split("]]")
        #print(toks2)
        for tt in toks2:
            x = re.search(r"Author: .*", tt)
            if(x != None):
                author = x.string.split("Author: ")[1]
                #print(author)
                tal.append(author)

    return tal

# Create initial list of all months, to be used as X axis
# Start Year = 2001
# End Year = 2024
td = {} # time dictionary (for easy lookup)
tl = [] # time list (for maintaining ordering)
year = 2001
month = 1
k = 0
for i in range(2001,2024):
    for j in range (1,13):
        t = '{:02d}/{}'.format(j,i)
        tl.append(t)
        td[t] = k
        k+=1

#print(td)


# Get an initial list of all authors, to be used as the Y axis
ad = {} # author dictionary

paper_count = 0

filepath = './bt-timeline.html'
with open(filepath, 'r') as fp:
    line = fp.readline()
    while line:
        if(re.search("created\":\".*?\",\"text\"", line) != None):
            paper_count+=1
            #print(line)
            toks = line.split("[[")
            for t in toks:
                toks2 = t.split("]]")
                for tt in toks2:
                    #print(tt)
                    x = re.search(r"Author: .*", tt)
                    if(x != None):
#                        print(x.string)
                        y = x.string.split("Author: ")
                        ad[y[1]] = 1
        line = fp.readline()
    fp.seek(0)

#print(ad.keys())

# Update the author dictionary to make it so that the value for each key
# is the index that will be used in the multi-dimensional CSV output array
# Create an author list (al) too for easier reverse lookup later
al = [] # global author list
i = 0
for k in ad.keys():
    al.append(k)
    ad[k] = i
    i+=1

#print(al)
#print(ad)

print("There are {} papers".format(paper_count))
print("There are {} Authors".format(len(ad.keys())))
print("There are {} months".format(len(td.keys())))

# Initialize the multi-dimensional array
# Note: In python it's a list of lists, so the first coordinate is really more like
# selecting a row (author), not a date (column) as it will appear in the final CSV
rows = len(ad.keys())
cols = len(td.keys())
arr = [[0 for i in range(cols)] for j in range(rows)]

# Go back to the beginning of the file, and this time we're going to increment
# the authorship for all columns of a given row, starting at a given [date,author]
with open(filepath, 'r') as fp2:
    line = fp2.readline()
    while line:
        if(re.search("created\":\".*?\",\"text\"", line) != None):
#            print(line)
            date = extract_month_year(line)
#            print(date)
            x_date = td[date]

            # Uncomment this to see lines which may be missing dates
            # (or they could just be system tiddlers, since the search criteria 
            # isn't specific enough)
#            if(x_date == "01/2001"):
#                print(line)

            # Now for each author we find, we're going to increment their publications count
            # for this month, and every future month, until the end of the timeline
            tal = extract_author_list(line)
            for author in tal:
#                print(author)
                y_author = ad[author]

#                print("date {} Author {} = {},{}".format(date, author, x_date, y_author))
                for x in range (x_date, cols):
#                    print("incrementing [{}],[{}]".format(y_author, x))
                    arr[y_author][x] += 1

        line = fp2.readline()

# Finally, output the array to a CSV file to consumption by other animation tools

with open("bar_chart_race.csv", 'w') as f:

    # First print out the column headings
    dates = ",".join(tl)
    print("Author,", end='', file=f),
    print(dates, file=f)

    i = 0
    for row in arr:
        print("{},".format(al[i]), end='', file=f),
        rowstr = ",".join(map(str,row))
        print(rowstr, file=f)
        i+=1
